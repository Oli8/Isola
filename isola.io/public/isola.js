$(document).ready(function(){

    var t = 0;
    var hasMoved = false;
    var gameOver = false;
    var gameId = 0;
    var localPlayer;
    var turn = 1;
    var myTurn = false;
    var grid = [
    [0,0,0,1,0,0,0],
    [0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0],
    [0,0,0,2,0,0,0]];

    socket.on('playerId', function(id){
        localPlayer = id;
        myTurn = (turn == localPlayer);
        alert("I'm player "+id +" "+myTurn);
    });

    socket.on('start', function(){
        $('.gameInfo').hide();
    });

    socket.on('destroying', function(data){
        destroy(data.index, data.x, data.y, 0, data.player);
        myTurn = !myTurn;
    });

    socket.on('gameOver', function(data){
        alert(data + ' has won !');
    });

    socket.on('moving', function(data){
        console.log(data);
        move(data.index, data.player);
    });

    $("body").append('<div id="container"></div>');

    function displayGrid(){
        $("#container").empty();
        
        for (var i = 0; i < grid.length; i++)
            for (var j = 0; j < grid[i].length; j++)
                if(grid[i][j] == 1 || grid[i][j] == 2)
                    $("#container").append("<div class='case'><div class='player" + grid[i][j] + "'></div></div>");
                else
                    $("#container").append("<div class='case'></div>");  
    }

    displayGrid();
    $('.token1').css({left: $('.case').eq(3).offset().left+8, top: $('.case').eq(3).offset().top+24});
    $('.token2').css({left: $('.case').eq(45).offset().left+8, top: $('.case').eq(45).offset().top+24});

    function getPosition(player){
        var c = 0;
        for (var i = 0; i < grid.length; i++)
            for (var j = 0; j < grid.length; j++)
            {
                if(grid[i][j] == player)
                    return [i, j, c];
                c++;
            }
    }

    function colorize(player){
        if(myTurn) return;
        var c = 0;
        for (var i = 0; i < grid.length; i++)
            for (var j = 0; j < grid.length; j++){   
                if(isPlayable(player, i, j)) 
                    $('.case').eq(c).attr('class' , "case playable" + player);
                else{
                    $('.case').eq(c).removeClass("playable1");
                    $('.case').eq(c).removeClass("playable2");
                }
                c++;
            }
    }

    function removeColor(){  
        $('.case').removeClass("playable1");
        $('.case').removeClass("playable2"); 
    }
    

    function isPlayable(player, x, y){
        var canPlay = false;
        if(grid[x][y] !== 0) return false;
        
        if(x < 6 )
        {
            if(grid[x+1][y] == player) canPlay = true;
            if(y < 6) 
                if(grid[x+1][y+1] == player) canPlay = true;
            if(y > 0)
                if(grid[x+1][y-1] == player) canPlay = true;
        }
        if(y<6) 
            if(grid[x][y+1] == player) canPlay = true;
        if( y > 0)
            if(grid[x][y-1] == player) canPlay = true;
        if(x > 0)
        {
            if(y < 6)
                if(grid[x-1][y+1] == player) canPlay = true;
            if( y > 0)
                if(grid[x-1][y-1] == player) canPlay = true;
            if(grid[x-1][y] == player) canPlay = true;
        }

        return canPlay;
    }

    function hasLost(player, x, y){
        var isOver = true;
        if(x < 6 )
        {
            if(grid[x+1][y] === 0) isOver = false;
            if(y < 6) 
                if(grid[x+1][y+1] === 0) isOver = false;
            if(y > 0)
                if(grid[x+1][y-1] === 0) isOver = false;
        }
        if(y<6) 
            if(grid[x][y+1] === 0) isOver = false;
        if( y > 0)
            if(grid[x][y-1] === 0) isOver = false;
        if(x > 0)
        {
            if(y < 6)
                if(grid[x-1][y+1] === 0) isOver = false;
            if( y > 0)
                if(grid[x-1][y-1] === 0) isOver = false;
            if(grid[x-1][y] === 0) isOver = false;
        }

        return isOver;
    }

    function move(tileIndex, player){
        var left = $('.case').eq(tileIndex).offset().left;
        var top = $('.case').eq(tileIndex).offset().top;
        $('.token'+player).css({left: left+8, top: top+7});
    }

    function destroy(tileIndex, x, y, emit, player){
        var tile = $('.case').eq(tileIndex);
        if (tile.attr('class').indexOf('destroyed') === -1 && tile.children().attr('class') === undefined){   
            if(emit) socket.emit('destroying', {player: player, index: tileIndex, x: x, y:y});
            grid[x][y] = 'D';
            hasMoved = false;
            tile.toggleClass("destroyed " + "element" + player);
            t++;
            p = (player == 1) ? '2' : '1';
            $('.info').html('<span id="p' + p + '">Player ' + p + ' has to move !</span>');
            colorize(p);
        }
    }

    $('.info').html('<span id = "p1">Player 1 has to move !</span>');

    colorize(1);

    $('.case').click(function() {
        if(gameOver) return false;

        player = (t % 2 === 0) ? 1 : 2; 
        var caseClass = $('.case').eq($(this).index()).attr('class');
        var x = Math.floor($(this).index() / 7);
        var y = $(this).index() % 7;

        if(!hasMoved){
            if (isPlayable(player, x, y)){
                removeColor();
                var oldX = getPosition(player)[0];
                var oldY = getPosition(player)[1];  
                var oldIndex = getPosition(player)[2];
                grid[oldX][oldY] = 0;      
                grid[x][y] = player;

                $('.case').eq(oldIndex).empty();
                $('.case').eq($(this).index()).append("<div class='player" + player + "'></div>");
                move($(this).index(), player);
                
                socket.emit('moving', {player: player, index: $(this).index()});

                hasMoved = true;
                canMove = true;
                $('.info').html('<span id="p' + player + '">Player ' +  player + ' has to destroy a tile !');
            }   
        }
        else{   
            destroy($(this).index(), x, y, 1, player);
        }    

        if(hasLost(1, getPosition(1)[0], getPosition(1)[1]) === true){
            socket.emit('gameOver', 2);
            $('.info').html('<span id="p2">Player 2 has won !</span>');
            gameOver = true;
        }
            

        if(hasLost(2, getPosition(2)[0], getPosition(2)[1]) === true){
            socket.emit('gameOver', 1);
            $('.info').html('<span id="p1">Player 1 has won !</span>');
            gameOver = true;
        }

    });

    $('#new').click(function() {
        location.reload();
    });

});