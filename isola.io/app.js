var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');

var player = 0;

app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res){
	res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
	console.log('a user connected');
	player++;
	socket.emit('playerId', player);

	if(player == 2){
		console.log('Game can begins !');
		console.log('Game id ' +uniquid());
		io.emit('start');
	}
	else 
		console.log(2-player +'player(s) needed to start');

	socket.on('disconnect', function(){
		console.log('user disconnected');
		player--;
	});

	socket.on('moving', function(data){
		console.log('Player '+data.player +' moved to tile number '+data.index);
		io.emit('moving', data);
	});

	socket.on('destroying', function(data){
		console.log('Player '+data.player +' destroyed tile number '+data.index);
		io.emit('destroying', {index: data.index, x:data.x, y:data.y, player:data.player});
	});

	socket.on('gameOver', function(player){
		console.log(player +' has won');
		io.emit('gameOver', player);
	});
});

http.listen(3000, function(){
	player = 0;
	console.log('listening on *:3000');
});

function uniquid(){
	return (new Date().getTime()).toString(16);
}